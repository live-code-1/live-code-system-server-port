package com.example.livecodeservicetwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class LiveCodeServiceTwoApplication {

    public static void main(String[] args) {
//        SpringApplication app = new SpringApplication(LiveCodeServiceTwoApplication.class);
//        app.setDefaultProperties(Collections.singletonMap("server.port","8082"));
//        app.run(args);
        SpringApplication.run(LiveCodeServiceTwoApplication.class, args);
    }

}
