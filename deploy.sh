# maven build service one and service two
mvn -f ./pom.xml clean package -Dmaven.test.skip=true

# docker build service one image
docker build -t live-code-service-one:latest ./live-code-service-one

# docker build service two image
docker build -t live-code-service-two:latest ./live-code-service-two

# docker-compose up (background)
docker-compose up -d