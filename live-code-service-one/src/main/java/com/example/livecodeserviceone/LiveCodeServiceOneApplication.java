package com.example.livecodeserviceone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiveCodeServiceOneApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiveCodeServiceOneApplication.class, args);
    }

}
